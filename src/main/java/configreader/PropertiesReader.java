package configreader;

import org.aeonbits.owner.Config;

@Config.LoadPolicy(Config.LoadType.MERGE)
@Config.Sources({
        "system:properties",
        "file:src/main/resources/config.properties",
})

public interface PropertiesReader extends Config {

    @Key("BROWSER")
    String browser();

    @Key("BASE_URL")
    String baseUrl();

    @Key("SIZE")
    String size();

    @Key("HEADLESS")
    Boolean headless();

    @Key("SCREENSHOTS")
    Boolean screenshots();

    @Key("TIMEOUT")
    int timeout();

    @Key("DURATION")
    int duration();

}
